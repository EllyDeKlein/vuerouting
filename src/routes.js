import { createRouter, createWebHistory } from 'vue-router';

import VuePage from './pages/Vue.vue';
import CSS from './pages/CSS.vue';
import JavaScript from './pages/JavaScript.vue';
import Laravel from './pages/Laravel.vue';
import HTML from './pages/HTML.vue';

const routes = [
    { path: '/JavaScript', component: JavaScript },
    { path: '/Laravel', component: Laravel },
    { path: '/CSS', component: CSS },
    { path: '/HTML', component: HTML },
    { path: '/Vue', component: VuePage }
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})

export default router